# GYMVOD

GYMVOD programovací seminář Septimy 2020/2021

## Učitelé

**David Hájek**

- hajekdd@gmail.com
- FEL
- Java, php , python, javascript

**Honza Borecký**
- FEL

### TEST
- 20.11.2020! teorie i implemetace
- 08.01.2021! teorie i implemetace
- 12.03.2021! teorie i implemetace

### Soutěž Kasiopea
- https://kasiopea.matfyz.cz/

### Distanční výuka
- Google classroom - **hjqghts**


### Pravidla

- Všichni se naučíme pracovat s GITem. Materiály budou na GitLabu.
- Discord - https://discord.gg/m4gQJhE
- Vždycky si to z něho na začátku hodiny stáhneme.
- Úkoly budou každý týden nebo ob týden. Na vypracování máte týden,
s dobrou výmluvou máte dva týdny. Jinak dostanete úkol navíc. Za
úkoly se budou dostávat jedničky. Neopisovat, poznám to. Odevzdávat
na svůj repositář.
- Budete mít projekt na semestr ve dvojicích. Témata si představíme
příští hodinu.
- Písemky budou asi dvě - část teoretická a část praktická.
- Při hodině mi budete pomáhat u počítače.

### Harmonogram

| Datum | Učitel | Téma | Přednášky | Úkol |
| --- | --- | --- | --- | --- |
| 4.9.2020 | David | Úvod; seznámení; stránka; úkoly; semestrálka; postup při řešení problému; vlastnosti algoritmu, základní struktury; nějaká basic úloha | | |
| 11.9.2020 | David | GIT, založit repo, nasdílet, ssh klíč; pull, add ., commit, push, rebase, merge | | GitLab - hw01 |
| 18.9.2020 | David | zápis algoritmu, programovací jazyky a rozdíly mezi nimi; překladače, typy překladačů; zadání semestrálky  | [lec3](https://gitlab.com/gymvod-group/septimy-2020/-/blob/master/lectures/2020-09-18/prezentace03.pdf) | [hw01](https://gitlab.com/gymvod-group/septimy-2020/-/tree/master/homework/2020-09-11) |
| 25.9.2020 | David | datové typy hodnotové a referenční; předdefinované datové typy; převody mezi datovými typy a jejich využití; deklarace proměnných  | [lec4](https://gitlab.com/gymvod-group/septimy-2020/-/blob/master/lectures/2020-09-25/prezentace04.pdf) | [hw02](https://gitlab.com/gymvod-group/septimy-2020/-/tree/master/homework/2020-09-25) |
| 2.10.2020 | David | základní struktury programovacího jazyka | [lec5](https://gitlab.com/gymvod-group/septimy-2020/-/blob/master/lectures/2020-10-02/prezentace05.pdf) | |
| 9.10.2020 | David | Cyklus a rekurze | [lec06](https://gitlab.com/gymvod-group/septimy-2020/-/blob/master/lectures/2020-10-09/prezentace06.pdf) | [hw03](https://gitlab.com/gymvod-group/septimy-2020/-/tree/master/homework/2020-10-09) |
| 16.10.2020 | David | Rekurze 2 a seznamy | [lec07](https://gitlab.com/gymvod-group/septimy-2020/-/blob/master/lectures/2020-10-16/prezentace07.pdf) | [hw04](https://gitlab.com/gymvod-group/septimy-2020/-/tree/master/homework/2020-10-16) |
| 23.10.2020 | David | Práce se stringem | [lec08](https://gitlab.com/gymvod-group/septimy-2020/-/blob/master/lectures/2020-10-23/prezentace08.pdf) | |
| 30.10.2020 | David | *Podzimní prázdniny* | | |
| 6.11.2020 | David | Práce se souborem | [le09](https://gitlab.com/gymvod-group/septimy-2020/-/blob/master/lectures/2020-11-06/prezentace09.pdf) | [hw05](https://gitlab.com/gymvod-group/septimy-2020/-/tree/master/homework/2020-11-06) |
| 13.11.2020 | David | Algoritmy třídění a řazení | [le10](https://gitlab.com/gymvod-group/septimy-2020/-/blob/master/lectures/2020-11-13/prezentace10.pdf) | [hw06](https://gitlab.com/gymvod-group/septimy-2020/-/tree/master/homework/2020-11-13) |
| 20.11.2020 | David | **TEST** | | |
| 27.11.2020 | David | GUI + Insert Sort| [le11](https://gitlab.com/gymvod-group/septimy-2020/-/blob/master/lectures/2020-11-27/prezentace11.pdf) | [hw07](https://gitlab.com/gymvod-group/septimy-2020/-/tree/master/homework/2020-11-27) |
| 4.12.2020 | David | Paint | [le12](https://gitlab.com/gymvod-group/septimy-2020/-/blob/master/lectures/2020-12-04/prezentace12.pdf) | |
| 11.12.2020 | David | OOP | [le13](https://gitlab.com/gymvod-group/septimy-2020/-/blob/master/lectures/2020-12-11/prezentace13b.pdf) | |
| 18.12.2020 | David | OOP 2 | [le14](https://gitlab.com/gymvod-group/septimy-2020/-/blob/master/lectures/2020-12-18/prezentace14.pdf) | |
| 25.12.2020 | David | *Vánoce* | | |
| 1.1.2021 | David | *Vánoce* | | |
| 8.1.2021 | David |  | | |
| 15.1.2021 | David |  | | |
| 22.1.2021 | David | Eratosthenovo síto | [le17](https://gitlab.com/gymvod-group/septimy-2020/-/blob/master/lectures/2021-01-22/prezentace17.pdf) | |
| 29.1.2021 | David |  | | |
|  |  |  |  |  |
| 5.2.2021 | David | Roman | [le18](https://gitlab.com/gymvod-group/septimy-2020/-/blob/master/lectures/2021-02-05/prezentace18.pdf) | [hw08](https://gitlab.com/gymvod-group/septimy-2020/-/tree/master/homework/2021-02-05) |
| 12.2.2021 | David | Aritmetika - soustavy | [le20](https://gitlab.com/gymvod-group/septimy-2020/-/blob/master/lectures/2021-02-12/prezentace20.pdf) | [hw09](https://gitlab.com/gymvod-group/septimy-2020/-/tree/master/homework/2021-02-12) |
| 19.2.2021 | David | Merge sort | [le21](https://gitlab.com/gymvod-group/septimy-2020/-/blob/master/lectures/2021-02-19/prezentace21.pdf) |  |
| 26.2.2021 | David | Uživatelské rozhraní | [le22](https://gitlab.com/gymvod-group/septimy-2020/-/blob/master/lectures/2021-02-26/prezentace22.pdf) | [hw10](https://gitlab.com/gymvod-group/septimy-2020/-/tree/master/homework/2021-02-26) |


### TEST výsledky

| Student | a | b | c | d | e | f | g | h | i | Teorie | Známka | 1 | 2 | 3 | 4 | 5 | 6 | 7 | Programko | Známka |
| --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
| 309 | 4 | 4 | 5 | 4 | 4 | 3 | 5 | 4 | 5 | 38 | 2 | 5 | 5 | 5 | 5 | 5 | 2 | 5 | 32 | 1- | 
| 12 | 5 | 4 | 5 | 5 | 4 | 1 | 3 | 5 | 5 | 37 | 2 | 5 | 4 | 8 | 0 | 5 | 4 |  | 26 | 3 | 
| 7 | 3 | 5 | 4 | 5 | 5 | 3 | 5 | 4 | 5 | 39 | 2 |  |  |  |  |  |  |  | 0 |  | 
| (-1)^(0.5) | 5 | 4 | 5 | 2 | 5 | 4 | 5 | 5 | 5 | 40 | 1 | 5 | 5 | 5 | 5 | 5 | 5 | 5 | 35 | 1 | 
| 27 | 5 | 5 | 5 | 5 | 5 | 3 | 5 | 5 | 5 | 43 | 1 | 5 | 5 | 10 | 5 | 5 | 5 |  | 35 | 1 | 
| 12 Matej:-) | 5 | 5 | 2 | 5 | 5 | 4 | 5 | 5 | 5 | 41 | 1 | 5 | 5 | 5 | 10 | 5 | 5 |  | 35 | 1 | 
| 54 | 5 | 5 | 4 | 4 | 5 | 4 | 3 | 4 | 4 | 38 | 2 | 5 | 5 | 0 | 5 | 5 | 5 | 0 | 25 | 3 | 
| 42 | 5 | 4 | 5 | 5 | 3 | 5 | 3 | 3 | 2 | 35 | 2 | 5 | 5 | 6 | 0 | 5 | 4 |  | 25 | 3 | 
| 8 | 5 | 4 | 3 | 4 | 5 | 5 | 5 | 4 | 5 | 40 | 1 | 5 | 5 | 5 | 4 | 5 | 2 |  | 26 | 3 | 
| 69696969420 | 5 | 4 | 5 | 3 | 5 | 3 | 5 | 4 | 5 | 39 | 2 | 5 | 5 | 0 | 5 | 5 | 5 | 4 | 29 | 2- | 
| 87 | 5 | 5 | 5 | 5 | 3 | 4 | 4 | 5 | 4 | 40 | 1 | 3 | 5 | 5 | 0 | 5 | 4 |  | 22 | 4 | 
