﻿namespace Součty_tržeb
{
    partial class oknoProgramu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dataGridViewTable = new System.Windows.Forms.DataGridView();
            this.mainMenu = new System.Windows.Forms.MenuStrip();
            this.souborToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nabídkaSouborOtevřít = new System.Windows.Forms.ToolStripMenuItem();
            this.nabídkaSouborOddělovač = new System.Windows.Forms.ToolStripSeparator();
            this.nabídkaSouborKonec = new System.Windows.Forms.ToolStripMenuItem();
            this.nabídkaOperace = new System.Windows.Forms.ToolStripMenuItem();
            this.nabídkaSpočtiSoučty = new System.Windows.Forms.ToolStripMenuItem();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewTable)).BeginInit();
            this.mainMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridViewTable
            // 
            this.dataGridViewTable.AllowUserToAddRows = false;
            this.dataGridViewTable.AllowUserToDeleteRows = false;
            this.dataGridViewTable.AllowUserToResizeColumns = false;
            this.dataGridViewTable.AllowUserToResizeRows = false;
            this.dataGridViewTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.ControlLight;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTable.DefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridViewTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewTable.Location = new System.Drawing.Point(0, 24);
            this.dataGridViewTable.Name = "dataGridViewTable";
            this.dataGridViewTable.RowHeadersVisible = false;
            this.dataGridViewTable.Size = new System.Drawing.Size(274, 247);
            this.dataGridViewTable.TabIndex = 0;
            // 
            // mainMenu
            // 
            this.mainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.souborToolStripMenuItem,
            this.nabídkaOperace});
            this.mainMenu.Location = new System.Drawing.Point(0, 0);
            this.mainMenu.Name = "mainMenu";
            this.mainMenu.Size = new System.Drawing.Size(274, 24);
            this.mainMenu.TabIndex = 1;
            this.mainMenu.Text = "menuStrip1";
            // 
            // souborToolStripMenuItem
            // 
            this.souborToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nabídkaSouborOtevřít,
            this.nabídkaSouborOddělovač,
            this.nabídkaSouborKonec});
            this.souborToolStripMenuItem.Name = "souborToolStripMenuItem";
            this.souborToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
            this.souborToolStripMenuItem.Text = "&Soubor";
            // 
            // nabídkaSouborOtevřít
            // 
            this.nabídkaSouborOtevřít.Name = "nabídkaSouborOtevřít";
            this.nabídkaSouborOtevřít.Size = new System.Drawing.Size(110, 22);
            this.nabídkaSouborOtevřít.Text = "&Otevřít";
            this.nabídkaSouborOtevřít.Click += new System.EventHandler(this.nabídkaSouborOtevřít_Click);
            // 
            // nabídkaSouborOddělovač
            // 
            this.nabídkaSouborOddělovač.Name = "nabídkaSouborOddělovač";
            this.nabídkaSouborOddělovač.Size = new System.Drawing.Size(107, 6);
            // 
            // nabídkaSouborKonec
            // 
            this.nabídkaSouborKonec.Name = "nabídkaSouborKonec";
            this.nabídkaSouborKonec.Size = new System.Drawing.Size(110, 22);
            this.nabídkaSouborKonec.Text = "&Konec";
            this.nabídkaSouborKonec.Click += new System.EventHandler(this.nabídkaSouborKonec_Click);
            // 
            // nabídkaOperace
            // 
            this.nabídkaOperace.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nabídkaSpočtiSoučty});
            this.nabídkaOperace.Name = "nabídkaOperace";
            this.nabídkaOperace.Size = new System.Drawing.Size(63, 20);
            this.nabídkaOperace.Text = "&Operace";
            // 
            // nabídkaSpočtiSoučty
            // 
            this.nabídkaSpočtiSoučty.Name = "nabídkaSpočtiSoučty";
            this.nabídkaSpočtiSoučty.Size = new System.Drawing.Size(145, 22);
            this.nabídkaSpočtiSoučty.Text = "&Spočti součty";
            this.nabídkaSpočtiSoučty.Click += new System.EventHandler(this.nabídkaSpočtiSoučty_Click);
            // 
            // oknoProgramu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(274, 271);
            this.Controls.Add(this.dataGridViewTable);
            this.Controls.Add(this.mainMenu);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.mainMenu;
            this.MaximizeBox = false;
            this.Name = "oknoProgramu";
            this.Text = "Součty vydaju";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewTable)).EndInit();
            this.mainMenu.ResumeLayout(false);
            this.mainMenu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewTable;
        private System.Windows.Forms.MenuStrip mainMenu;
        private System.Windows.Forms.ToolStripMenuItem souborToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nabídkaSouborOtevřít;
        private System.Windows.Forms.ToolStripSeparator nabídkaSouborOddělovač;
        private System.Windows.Forms.ToolStripMenuItem nabídkaSouborKonec;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.ToolStripMenuItem nabídkaOperace;
        private System.Windows.Forms.ToolStripMenuItem nabídkaSpočtiSoučty;
    }
}

