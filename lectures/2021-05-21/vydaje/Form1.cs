﻿using System;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace Součty_tržeb
{
    public partial class oknoProgramu : Form
    {
        Color colorSum = Color.Bisque;
        Color colorSumSum = Color.Tomato;
        const int columnWidth = 55;
        
        public oknoProgramu()
        {
            InitializeComponent();
        }

        private void nabídkaSouborOtevřít_Click(object sender, EventArgs e)
        {
            // Otevření souboru
            if (openFileDialog.ShowDialog() != DialogResult.OK)
                return;
            string fileName = openFileDialog.FileName;
            StreamReader file = new StreamReader(fileName, Encoding.Default);

            // Záhlaví souboru a sloupce tabulky
            string headerColumn = file.ReadLine();
            string[] columnHeaders = headerColumn.Split(';');
            int columnCount = dataGridViewTable.ColumnCount = columnHeaders.Length + 1;
            for (int ic = 0; ic < columnCount; ic++)
            {
                DataGridViewColumn col = dataGridViewTable.Columns[ic];
                col.SortMode = DataGridViewColumnSortMode.NotSortable;
                if (ic > 0) // není první
                {
                    col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                    col.Width = columnWidth;
                    col.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                }
                if (ic < columnCount - 1) // není poslední
                    col.HeaderText = columnHeaders[ic];
            }

            // Poslední sloupec
            var lastColumn = dataGridViewTable.Columns[columnCount-1];
            lastColumn.DefaultCellStyle.BackColor = colorSum;
            lastColumn.ReadOnly = true;
            lastColumn.HeaderText = "Součty";
            lastColumn.DefaultCellStyle.Font = new Font(Font, FontStyle.Bold);
            lastColumn.HeaderCell.Style.Font = new Font(Font, FontStyle.Bold);

            // Řádky tabulky
            dataGridViewTable.RowCount = 0;
            string fileRow;
            while ((fileRow = file.ReadLine()) != null)
            {
                string[] items = fileRow.Split(';');
                dataGridViewTable.Rows.Add(items);
            }

            // Poslední řádek
            dataGridViewTable.Rows.Add();
            var lastRow = dataGridViewTable.Rows[dataGridViewTable.RowCount - 1];
            lastRow.DefaultCellStyle.BackColor = colorSum;
            lastRow.ReadOnly = true;
            lastRow.Cells[0].Value = "Součty";
            lastRow.DefaultCellStyle.Font = new Font(Font, FontStyle.Bold);
            lastRow.Cells[columnCount-1].Style.BackColor = colorSumSum;

            // Rozměry tabulky a podle toho rozměry okna
            int tableWidth = dataGridViewTable.Columns[0].Width + 
                (dataGridViewTable.ColumnCount - 1) * (columnWidth + 1);
            int tableHeight = dataGridViewTable.ColumnHeadersHeight + 
                dataGridViewTable.RowCount * (lastRow.Height + 1);
            int totalHeight = tableHeight + mainMenu.Height;
            ClientSize = new Size(tableWidth, totalHeight);

            // Zavření souboru
            file.Close();
        }

        private void nabídkaSouborKonec_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void nabídkaSpočtiSoučty_Click(object sender, EventArgs e)
        {
            // Jsou v tabulce data?
            if (dataGridViewTable.ColumnCount < 3)
            {
                MessageBox.Show("Tabulka není (správně) naplněná");
                return;
            }

            // Rozměry tabulky
            int columnCount = dataGridViewTable.ColumnCount;
            int rowCount = dataGridViewTable.RowCount;
            int indexLastColumn = columnCount - 1;
            int indexLastRow = rowCount - 1;

            // Řádkové součty
            int sumSum = 0;
            for (int ir = 0; ir < indexLastRow; ir++)
            {
                int sumRow = 0;
                for (int ic = 1; ic < indexLastColumn; ic++)
                {
                    // Pro jednoduchost neřešíme možnost Value rovné null či
                    //   nečíselné hodnoty v buňce
                    DataGridViewCell cell = dataGridViewTable[ic, ir];
                    int hodnota = Convert.ToInt32(cell.Value.ToString());
                    sumRow += hodnota;
                }
                dataGridViewTable[indexLastColumn, ir].Value = sumRow.ToString("N0");
                sumSum += sumRow;
            }

            // Sloupcové součty
            for (int ic = 1; ic < indexLastColumn; ic++)
            {
                int sumColumn = 0;
                for (int ir = 0; ir < indexLastRow; ir++)
                {
                    DataGridViewCell cell = dataGridViewTable[ic, ir];
                    int hodnota = Convert.ToInt32(cell.Value.ToString());
                    sumColumn += hodnota;
                }
                dataGridViewTable[ic, indexLastRow].Value = sumColumn.ToString("N0");
            }

            // Zobrazení součtu součtů
            dataGridViewTable[indexLastColumn, indexLastRow].Value = sumSum.ToString("N0");
        }
    }
}
