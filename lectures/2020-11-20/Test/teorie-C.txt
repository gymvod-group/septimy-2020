C
Na vypracování teoretické části máte 30 minut.
Některé odpovědi se mohou lišit v jiných programovacích jazycích, ptám se na C# a "svět" .NET Frameworku.

a) Jaké jsou tři vlastnosti algoritmu a co znamenají? (5b)

b) Jaké znáte pogramovací jazyky? Je c# kompilovaný nebo interpretovaný programovací jazyk? Rozveďte. (5b)

c) Popište dva datové typy, do kterých můžeme ukládat soubor (množinu) dat stejného datového typu, a popište, v čem se liší? (5b)

d) Co je to rekurze? Jaké znáte druhy rekurzí? Popište je. (5b)

e) Jak jsou realizovány řetězce v c#? Jak je v c# realizován znak? (5b)

f) Co je to implicitní konverze? Uveďte příklad implicitní konverze. Vysvětlete, kdy může nastat při konverzi Overflow exception. (5b)

g) Jak se jmenuje třída, která slouží pro čtení ze souboru? Kde se soubor nachází (relativní cesta)? (5b)

h) Jak definujeme funkci s návratovou hodnotou a parametrem a jak ji zavoláme? (5b)

i) Jaké znáte řadící algoritmy? (5b)






