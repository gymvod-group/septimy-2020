B
Na praktickou část máte čas do konce hodiny.
(55b)

1) Vygenerujte náhodné číslo.
2) Interval, ze kterého se generuje náhodné číslo, zadejte pomocí vstupu přes konzoli.
3) Zjistěte zdali je číslo prvočíslo.
4) Implementaci algoritmu na ověření prvočísla vložte do funkce.
5) Výsledek vypište do konzole.
6) Výsledek uložte do textového souboru.

