﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soubory
{
    class Program
    {
        static void Main(string[] args)
        {
            //Cviceni1();
            //Cviceni2();

            //PraceSeSouborem();
            PraceSeSouborem2();

            Console.ReadKey();
        }

        static void PraceSeSouborem()
        {
            StreamReader file = new StreamReader("text.txt");
            StringBuilder sb = new StringBuilder();
            string radek = file.ReadLine();
            while (radek != null)
            {
                sb.AppendLine(radek);
                radek = file.ReadLine();
            }
            file.Close();

            Console.WriteLine(sb);
        }

        static void PraceSeSouborem2()
        {
            StreamWriter file = new StreamWriter("textWrite.txt");
            string radek = "Ahoj z kodu";
            file.WriteLine(radek);
            file.Close();
        }


        //vytvorit retezec
        //Spocitat vyskyt znaku a
        static void Cviceni1()
        {
            string retez = "fdjsaf hjsdkafhdjk d sajfkdh fdsalf da fjdsahfsaf lkjfklsaj fdsla jkdfslajf";
            char znak = 'a';
            int kolikrat = 0;
            //Console.WriteLine(retez[0]);
            for (int i = 0; i < retez.Length; i++)
            {
                if (retez[i] == znak)
                {
                    //kolikrat += 1;
                    kolikrat++;
                }
            }

            Console.WriteLine($"Pismeno {znak} se v textu vyskytuje {kolikrat}");
        }

        static void Cviceni2()
        {
            //nahodny reteze o delce 100 znaku
            StringBuilder sb = new StringBuilder();
            Random rnd = new Random();
            for (int i = 0; i < 100; i++)
            {
                sb.Append((char) rnd.Next(65, 90));
            }
            Console.WriteLine(sb);

            char znak = 'A';
            int kolikrat = 0;
            for (int i = 0; i < sb.Length; i++)
            {
                if (sb[i] == znak)
                {
                    kolikrat++;
                }
            }

            Console.WriteLine($"Pismeno {znak} se v textu vyskytuje {kolikrat}");
        }

    }
}
