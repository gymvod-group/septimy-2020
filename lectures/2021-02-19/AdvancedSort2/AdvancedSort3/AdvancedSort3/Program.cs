﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdvancedSort3
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr = new int[] {38, 27, 43, 3, 9, 82, 10};
            Console.WriteLine("Given array");
            Console.WriteLine(String.Join(", ", arr));


            MergeSort ms = new MergeSort();
            ms.Sort(arr, 0, arr.Length - 1);

            Console.WriteLine();
            Console.WriteLine("Sorted array");
            Console.WriteLine(String.Join(", ", arr));

            Console.ReadKey();


        }
    }
}
