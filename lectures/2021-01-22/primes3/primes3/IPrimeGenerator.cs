﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace primes3
{
    interface IPrimeGenerator
    {
        List<int> PrimesFromIntervalOldWay();

        List<int> PrimesFromIntervalEratosthen();
    }
}
