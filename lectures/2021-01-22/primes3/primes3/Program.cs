﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace primes3
{
    class Program
    {
        static void Main(string[] args)
        {
            int start = ReadNumber("Dolni mez");
            int end = ReadNumber("Horni mez");

            Stopwatch watch = new Stopwatch();
            PrimeGenerator generator = new PrimeGenerator(start, end);


            watch.Start();
            List<int> primes = generator.PrimesFromIntervalOldWay();
            //Console.WriteLine(String.Join(", ", primes));
            watch.Stop();
            Console.WriteLine(watch.Elapsed.TotalMilliseconds + "ms");


            watch.Reset();
            watch.Start();
            List<int> primes2 = generator.PrimesFromIntervalEratosthen();
            //Console.WriteLine(String.Join(", ", primes2));
            watch.Stop();
            Console.WriteLine(watch.Elapsed.TotalMilliseconds + "ms");






            Console.ReadKey();
        }



        static int ReadNumber(string name)
        {
            int number;
            Console.WriteLine("Zadejte " + name);
            while (!int.TryParse(Console.ReadLine(), out number))
                Console.WriteLine("Zadejte znovu " + name);
            
            return number;
        }
    }
}
