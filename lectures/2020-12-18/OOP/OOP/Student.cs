﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP
{
    class Student : Person
    {
        public Student(int x, int y, string name) : base(x, y, name)
        {
            base.job = "Student";
        }
    }
}
