﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Snowstorm
{
    class Snowflake
    {
        private float x, y;
        private int size;

        public Snowflake(float x, float y, int size)
        {
            this.x = x;
            this.y = y;
            this.size = size;
        }

        public void Draw(Graphics g)
        {
            Font pismo = new Font("Arial", size);
            g.DrawString("X", pismo, Brushes.White, this.x, y);
        }

        public void Move(float m, int size)
        {
            this.y += m;
            this.size = size;
        }


        public void ChangeSize(int s)
        {
            this.size += s;
        }
    }
}
