﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oopOpakovani
{
    class Circle : IShape
    {
        private decimal r;

        public Circle(decimal r)
        {
            this.r = r;
        }

        public decimal CalculateArea()
        {
            return Convert.ToDecimal(Math.PI) * r * r;
        }

        public string GetInfo()
        {
            string m = "Ja jsem kruh a muj polomer je " + r;
            m = $"Ja jsem kruh a muj polomer je {r}";
            return m;
        }
    }
}
