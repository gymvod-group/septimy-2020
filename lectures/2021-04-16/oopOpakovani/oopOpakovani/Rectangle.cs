﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oopOpakovani
{
    class Rectangle : IShape
    {
        private decimal a;
        private decimal b;

        public Rectangle(decimal a, decimal b)
        {
            this.a = a;
            this.b = b;
        }

        public decimal CalculateArea()
        {
            return a * b;
        }

        public virtual string GetInfo()
        {
            return "Ja jsem obdelnik";
        }
    }
}
