﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace oopOpakovani
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            Random rnd = new Random();
            List<IShape> shapes = new List<IShape>();


            for (int i = 0; i < 20; i++)
            {
                int n = rnd.Next(0, 3);
                switch (n)
                {
                    case 0:
                        shapes.Add(new Circle(5));
                        break;
                    case 1:
                        shapes.Add(new Square(10));
                        break;
                    case 2:
                        shapes.Add(new Rectangle(6, 8));
                        break;
                }
            }



            foreach (IShape item in shapes)
            {
                textBox1.Text += item.GetInfo() + Environment.NewLine;
            }


        }
    }
}
