﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace morseovka
{
    public partial class Form1 : Form
    {
        MorseTranslator mt = new MorseTranslator();

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            textBoxOut.Text = mt.FromMorse(textBoxIn.Text);
        }

        private void buttonMorse_Click(object sender, EventArgs e)
        {
            textBoxOut.Text = mt.ToMorse(textBoxIn.Text);
        }
    }
}
