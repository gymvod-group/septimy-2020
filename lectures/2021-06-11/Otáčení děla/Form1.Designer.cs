﻿namespace Otáčení_děla
{
    partial class oknoProgramu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.playerA = new System.Windows.Forms.Label();
            this.playerScoreA = new System.Windows.Forms.Label();
            this.playerFiredA = new System.Windows.Forms.Label();
            this.playerFiredB = new System.Windows.Forms.Label();
            this.playerScoreB = new System.Windows.Forms.Label();
            this.playerB = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 10;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // playerA
            // 
            this.playerA.AutoSize = true;
            this.playerA.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.playerA.Location = new System.Drawing.Point(4, 3);
            this.playerA.Name = "playerA";
            this.playerA.Size = new System.Drawing.Size(58, 13);
            this.playerA.TabIndex = 0;
            this.playerA.Text = "Player A:";
            // 
            // playerScoreA
            // 
            this.playerScoreA.AutoSize = true;
            this.playerScoreA.Location = new System.Drawing.Point(69, 3);
            this.playerScoreA.Name = "playerScoreA";
            this.playerScoreA.Size = new System.Drawing.Size(13, 13);
            this.playerScoreA.TabIndex = 1;
            this.playerScoreA.Text = "0";
            // 
            // playerFiredA
            // 
            this.playerFiredA.AutoSize = true;
            this.playerFiredA.Location = new System.Drawing.Point(7, 20);
            this.playerFiredA.Name = "playerFiredA";
            this.playerFiredA.Size = new System.Drawing.Size(0, 13);
            this.playerFiredA.TabIndex = 2;
            // 
            // playerFiredB
            // 
            this.playerFiredB.AutoSize = true;
            this.playerFiredB.Location = new System.Drawing.Point(513, 20);
            this.playerFiredB.Name = "playerFiredB";
            this.playerFiredB.Size = new System.Drawing.Size(0, 13);
            this.playerFiredB.TabIndex = 5;
            // 
            // playerScoreB
            // 
            this.playerScoreB.AutoSize = true;
            this.playerScoreB.Location = new System.Drawing.Point(575, 3);
            this.playerScoreB.Name = "playerScoreB";
            this.playerScoreB.Size = new System.Drawing.Size(13, 13);
            this.playerScoreB.TabIndex = 4;
            this.playerScoreB.Text = "0";
            // 
            // playerB
            // 
            this.playerB.AutoSize = true;
            this.playerB.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.playerB.Location = new System.Drawing.Point(510, 3);
            this.playerB.Name = "playerB";
            this.playerB.Size = new System.Drawing.Size(58, 13);
            this.playerB.TabIndex = 3;
            this.playerB.Text = "Player B:";
            // 
            // oknoProgramu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(601, 200);
            this.Controls.Add(this.playerFiredB);
            this.Controls.Add(this.playerScoreB);
            this.Controls.Add(this.playerB);
            this.Controls.Add(this.playerFiredA);
            this.Controls.Add(this.playerScoreA);
            this.Controls.Add(this.playerA);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "oknoProgramu";
            this.Text = "Otáčení děla";
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.oknoProgramu_Paint);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.oknoProgramu_KeyDown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label playerA;
        private System.Windows.Forms.Label playerScoreA;
        private System.Windows.Forms.Label playerFiredA;
        private System.Windows.Forms.Label playerFiredB;
        private System.Windows.Forms.Label playerScoreB;
        private System.Windows.Forms.Label playerB;
    }
}

