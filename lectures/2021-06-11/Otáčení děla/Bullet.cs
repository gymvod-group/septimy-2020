﻿using System;
using System.Drawing;

namespace Otáčení_děla
{
    class Bullet : Location, IDrawable
    {
        public double Vx { get; set; }
        public double Vy { get; set; }
        public int V { get; private set; }
        public int Radius { get; private set; }

        public Bullet(double x, double y)
        {
            Vx = 0;
            Vy = 0;
            V = 2;
            Radius = 3;
            this.X = x;
            this.Y = y;
        }

        public void Move()
        {
            X += Vx;
            Y += Vy;
        }

        public void Reset()
        {
            X = -20;
            Vx = 0; Vy = 0;
        }

        public void CalculateVector(double cannonEndX, double cannonEndY, double angle)
        {
            X = cannonEndX;
            Y = cannonEndY;
            double angleB = angle * Math.PI / 180;
            Vx = V * Math.Cos(angleB);
            Vy = V * Math.Sin(angleB);
        }

        public void Draw(Graphics g)
        {
            int xLTBullet = Convert.ToInt32(X) - Radius;
            int yLTBullet = Convert.ToInt32(Y) - Radius;
            g.FillEllipse(Brushes.Black, xLTBullet, yLTBullet, 2 * Radius, 2 * Radius);
        }

        public bool IsHit(Target target)
        {
            double xBulletMiddle = X + Radius;
            double yBulletMiddle = Y + Radius;

            return (xBulletMiddle > target.X - target.Radius && xBulletMiddle < target.X + target.Radius) && (yBulletMiddle > target.Y - target.Radius && yBulletMiddle < target.Y + target.Radius);
        }

        public bool IsOut(Size screen)
        {
            return (X < 0 + 2*Radius || Y < 0 + 2*Radius || X > screen.Width || Y > screen.Height);
        }
    }
}
