﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Otáčení_děla
{
    public partial class oknoProgramu : Form
    {
        // Úhel natočení dělové hlavně ve STUPNÍCH - základní 
        //   charakteristika pozice děla
        double úhel = 0; 

        // Rozměry děla
        int poloměrKuličky =  5;
        int délkaHlavně    = 30;
        int tloušťkaHlavně =  5;

        public oknoProgramu()
        {
            InitializeComponent();
        }

        private void oknoProgramu_Paint(object sender, PaintEventArgs e)
        {
            Graphics kp = e.Graphics;

            // Střed děla = střed okna
            int xStředu = ClientSize.Width  / 2;
            int yStředu = ClientSize.Height / 2;

            // Nakreslím střed děla jako kuličku
            int xLH = xStředu - poloměrKuličky;
            int yLH = yStředu - poloměrKuličky;
            int šířka = 2*poloměrKuličky;
            int výška = šířka;
            kp.FillEllipse(Brushes.Black, xLH, yLH, šířka, výška);

            // Nakreslím hlaveň jako tlustou úsečku
            double úhelRad = úhel * Math.PI / 180;
            double deltaX = délkaHlavně * Math.Cos(úhelRad);
            double deltaY = délkaHlavně * Math.Sin(úhelRad);
            int xKonce = Convert.ToInt32(xStředu + deltaX);
            int yKonce = Convert.ToInt32(yStředu + deltaY);
            Pen pero = new Pen(Color.Black, tloušťkaHlavně);
            kp.DrawLine(pero, xStředu, yStředu, xKonce, yKonce);
        }

        private void oknoProgramu_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            { 
                case Keys.Left:  // Otoč proti hodinkám
                    úhel -= 22.5;
                    break;
                case Keys.Right: // Otoč ve směru hodinek
                    úhel += 22.5;
                    break;
            }
            Refresh();
        }
    }
}
