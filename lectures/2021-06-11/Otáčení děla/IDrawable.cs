﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace Otáčení_děla
{
    interface IDrawable
    {
        void Draw(Graphics g);
    }
}
