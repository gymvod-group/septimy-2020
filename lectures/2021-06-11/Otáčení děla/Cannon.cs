﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace Otáčení_děla
{
    class Cannon : Location, IDrawable
    {
        public int EndX { get; set; }
        public int EndY { get; set; }
        public double Angle { get; set; }
        public int Score { get; set; }
        public int FiredBullets { get; set; }

        private int CircleRadius = 5;
        private int Length = 30;
        private int Width = 5;
        
        public Cannon(int xMiddle, int yMiddle)
        {
            Angle = 0;
            Score = 0;
            FiredBullets = 0;
            X = xMiddle;
            Y = yMiddle;
        }

        public void Draw(Graphics g)
        {
            int xLH = Convert.ToInt32(X) - CircleRadius;
            int yLH = Convert.ToInt32(Y) - CircleRadius;
            g.FillEllipse(Brushes.Black, xLH, yLH, 2 * CircleRadius, 2 * CircleRadius);
            calculateCannonEnd();
            Pen pero = new Pen(Color.Black, Width);
            g.DrawLine(pero, Convert.ToInt32(X), Convert.ToInt32(Y), EndX, EndY);
        }

        private void calculateCannonEnd()
        {
            double angleRad = Angle * Math.PI / 180;
            double deltaX = Length * Math.Cos(angleRad);
            double deltaY = Length * Math.Sin(angleRad);
            EndX = Convert.ToInt32(X + deltaX);
            EndY = Convert.ToInt32(Y + deltaY);
        }

    }
}
