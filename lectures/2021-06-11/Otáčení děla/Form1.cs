﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Otáčení_děla
{
    public partial class oknoProgramu : Form
    {
        int playing = 0;
        Cannon[] cannons = new Cannon[2];
        List<Bullet> bullets = new List<Bullet>();
        Target target;

        public oknoProgramu()
        {
            InitializeComponent();

            cannons[0] = new Cannon(ClientSize.Width / 4, ClientSize.Height / 2);
            cannons[1] = new Cannon(ClientSize.Width / 4 * 3, ClientSize.Height / 2);
            target = new Target(ClientSize.Width, ClientSize.Height);
        }

        private void oknoProgramu_Paint(object sender, PaintEventArgs e)
        {
            Graphics kp = e.Graphics;

            target.Draw(kp);
            foreach (IDrawable item in cannons) item.Draw(kp);
            foreach (IDrawable item in bullets) item.Draw(kp);
        }

        private void oknoProgramu_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            { 
                case Keys.Left:
                    cannons[playing].Angle -= 3;
                    break;
                case Keys.Right:
                    cannons[playing].Angle += 3;
                    break;
                case Keys.Space:
                    if (cannons[playing].FiredBullets < 10)
                    {
                        Bullet bullet = new Bullet(cannons[playing].EndX, cannons[playing].EndY);
                        bullet.CalculateVector(cannons[playing].EndX, cannons[playing].EndY, cannons[playing].Angle);
                        bullets.Add(bullet);
                        cannons[playing].FiredBullets++;
                    }

                    break;
            }

            Refresh();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            // Započti přitažlivost Země
            //vyStřely += tíhovéZrychlení * čas;

            for (int i = 0; i < bullets.Count; i++)
            {
                Bullet bullet = bullets[i];
                bullet.Move();
                bullet.Vy += 0.005;
                
                if (bullet.IsHit(target))
                {
                    bullets.RemoveAt(i);
                    target.Reset(ClientSize);
                    cannons[playing].Score += 10 - cannons[playing].FiredBullets;
                }

                if(bullet.IsOut(ClientSize))
                {
                    bullets.RemoveAt(i);
                }
            }

            if (cannons[playing].FiredBullets == 10 && bullets.Count == 0) //change player
            {
                playing = (playing + 1) % 2;
                cannons[playing].FiredBullets = 0;
            }

            playerScoreA.Text = cannons[0].Score.ToString();
            playerScoreB.Text = cannons[1].Score.ToString();
            playerFiredA.Text = (10 - cannons[0].FiredBullets).ToString();
            playerFiredB.Text = (10 - cannons[1].FiredBullets).ToString();


            Refresh();
        }
    }
}
