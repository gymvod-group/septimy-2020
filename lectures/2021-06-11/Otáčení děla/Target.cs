﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace Otáčení_děla
{
    class Target : Location, IDrawable
    {
        private Random rnd = new Random();
        public int Radius { get; private set; }

        public Target(double x, double y)
        {
            Radius = 10;
            this.X = x - Radius;
            this.Y = y - Radius;
        }

        public void Draw(Graphics g)
        {
            int xLTBullet = Convert.ToInt32(X) - Radius;
            int yLTBullet = Convert.ToInt32(Y) - Radius;
            g.DrawEllipse(Pens.CornflowerBlue, xLTBullet, yLTBullet, 2 * Radius, 2 * Radius);
        }

        public void Reset(Size screen)
        {
            X = rnd.Next(Radius, screen.Width - Radius);
            Y = rnd.Next(Radius, screen.Height - Radius);
        }
    }
}
