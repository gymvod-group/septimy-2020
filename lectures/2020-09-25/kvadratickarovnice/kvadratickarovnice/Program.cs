﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kvadratickarovnice
{
    class Program
    {
        static void Main(string[] args)
        {

            int a = ReadNumber("a");
            int b = ReadNumber("b");
            int c = ReadNumber("c");

            int discriminant = b * b - 4 * a * c;
            if (discriminant < 0)
            {
                Console.WriteLine("Rovnice nema reseni v realnych cislech");
                Console.ReadKey();
                return;
            }


            if (discriminant == 0)
            {
                double x = (-b) / (2 * a);
                CheckRoot(new int[3] { a, b, c }, x);
                Console.WriteLine("Rovnice ma jeden koren x = " + x);
            }

            if (discriminant > 0)
            {
                double x1 = (-b + Math.Sqrt(discriminant)) / (2 * a);
                double x2 = (-b - Math.Sqrt(discriminant)) / (2 * a);

                CheckRoot(new int[3] { a, b, c }, x1);
                CheckRoot(new int[3] { a, b, c }, x2);
                Console.WriteLine("Kvadraticka rovnice ma dva koreny x1={0}, a x2={1}", x1, x2);
            }


            Console.ReadKey();
        }


        static bool CheckRoot(int[] coefs, double x)
        {
            double result = coefs[0] * Math.Pow(x, 2) + coefs[1] * x + coefs[2];

            if (result != 0D)
            {
                Console.WriteLine("Neco je spatne");
            }

            return true;
        }

        static int ReadNumber(string name)
        {

            Console.WriteLine("Zadej cislo " + name);
            int number;

            bool success = int.TryParse(Console.ReadLine(), out number);
            while(!success)
            //while(!int.TryParse(Console.ReadLine(), out number))
            {
                Console.WriteLine("Zadej znovu cislo " + name);
                success = int.TryParse(Console.ReadLine(), out number);

            }

            return number;
        }
    }
}
