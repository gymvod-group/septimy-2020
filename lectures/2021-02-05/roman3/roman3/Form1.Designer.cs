﻿namespace roman3
{
    partial class Form1
    {
        /// <summary>
        /// Vyžaduje se proměnná návrháře.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Uvolněte všechny používané prostředky.
        /// </summary>
        /// <param name="disposing">hodnota true, když by se měl spravovaný prostředek odstranit; jinak false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kód generovaný Návrhářem Windows Form

        /// <summary>
        /// Metoda vyžadovaná pro podporu Návrháře - neupravovat
        /// obsah této metody v editoru kódu.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.buttonToRoman = new System.Windows.Forms.Button();
            this.labelRoman = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(59, 394);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "label1";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(213, 64);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(276, 20);
            this.textBox1.TabIndex = 1;
            // 
            // buttonToRoman
            // 
            this.buttonToRoman.Location = new System.Drawing.Point(213, 105);
            this.buttonToRoman.Name = "buttonToRoman";
            this.buttonToRoman.Size = new System.Drawing.Size(114, 23);
            this.buttonToRoman.TabIndex = 2;
            this.buttonToRoman.Text = "Preved na rimske";
            this.buttonToRoman.UseVisualStyleBackColor = true;
            this.buttonToRoman.Click += new System.EventHandler(this.buttonToRoman_Click);
            // 
            // labelRoman
            // 
            this.labelRoman.AutoSize = true;
            this.labelRoman.Location = new System.Drawing.Point(254, 155);
            this.labelRoman.Name = "labelRoman";
            this.labelRoman.Size = new System.Drawing.Size(0, 13);
            this.labelRoman.TabIndex = 3;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(349, 105);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(140, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "Preved na cele";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.labelRoman);
            this.Controls.Add(this.buttonToRoman);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button buttonToRoman;
        private System.Windows.Forms.Label labelRoman;
        private System.Windows.Forms.Button button1;
    }
}

