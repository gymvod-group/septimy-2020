﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace roman3
{
    interface IRomanConverter
    {
        //
        // Summary:
        //     Converts number to a roman numeral.
        //
        // Parameters:
        //     number:
        //        A int containing a number to convert.
        //
        // Return:
        //     A string representing the roman numeral.
        string ToRoman(int number);

        //
        // Summary:
        //     Converts roman numeral to a number.
        //
        // Parameters:
        //     roman:
        //        A string containing a roman numeral to convert.
        //
        // Return:
        //     An int representing the converted number.
        int FromRoman(string roman);


    }

}
