﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace roman3
{
    public partial class Form1 : Form
    {
        IRomanConverter converter;

        public Form1()
        {
            InitializeComponent();

            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            dictionary.Add("po", "pondeli");
            dictionary.Add("ut", "utery");
            dictionary.Add("st", "streda");
            dictionary.Add("ct", "ctvrtek");
            dictionary.Add("pa", "patek");
            //dictionary.Add("pa", "patek");

            label1.Text = dictionary["pa"];

            if (dictionary.ContainsKey("so"))
                label1.Text = dictionary["so"];

            dictionary.Remove("pa");
            //label1.Text = dictionary["pa"];

            label1.Text = "";
            foreach (KeyValuePair<string, string> item in dictionary)
            {
                label1.Text += $"key={item.Key}, value={item.Value}; ";
            }


            converter = new Converter();
        }

        private void buttonToRoman_Click(object sender, EventArgs e)
        {
            string input = textBox1.Text;
            int number;
            if (!int.TryParse(input, out number))
                label1.Text = "Je to spatne..";


            labelRoman.Text = converter.ToRoman(number);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            labelRoman.Text = converter.FromRoman(textBox1.Text).ToString();
        }
    }
}
