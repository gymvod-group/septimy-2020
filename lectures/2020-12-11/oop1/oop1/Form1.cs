﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace oop1
{
    public partial class Form1 : Form
    {
        Person pepa;
        Teacher david;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            Graphics kp = e.Graphics;

            pepa = new Person(50, 50, "Pepa");
            pepa.Draw(kp);
            david = new Teacher(100, 50, "david");
            david.Draw(kp);

            Random rnd = new Random();
            for (int i = 0; i < 10; i++)
            {
                Person person = new Person(rnd.Next(0, Width), rnd.Next(0, Height), "Jmeno"+i);
                person.Draw(kp);
            }
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Space)
            {
                pepa.SayHi();
                david.SayHi();
            }
        }
    }
}
