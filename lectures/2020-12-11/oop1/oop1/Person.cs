﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace oop1
{
    class Person
    {
        private int x;
        private int y;
        private string name;

        protected string job;

        public Person(int x, int y, string name)
        {
            this.x = x;
            this.y = y;
            this.name = name;
        }

        public void Draw(Graphics kp)
        {
            Pen pen = new Pen(Color.Blue, 3);

            kp.FillEllipse(Brushes.Blue, x, y, 30, 30);

            kp.DrawLine(pen, x + 15, y + 30, x + 15, y + 60);

            //ruce
            kp.DrawLine(pen, x + 15, y + 30, x, y + 60);
            kp.DrawLine(pen, x + 15, y + 30, x + 30, y + 60);

            //nohy
            kp.DrawLine(pen, x + 15, y + 60, x, y + 100);
            kp.DrawLine(pen, x + 15, y + 60, x + 30, y + 100);

            //jmeno
            DrawName(kp);
        }

        public void SayHi()
        {
            MessageBox.Show($"{name} zdravi a jeho prace je {job}");
        }


        private void DrawName(Graphics kp)
        {
            Font drawFont = new Font("Arial", 16);
            SolidBrush drawBrush = new SolidBrush(Color.Black);
            StringFormat drawFormat = new StringFormat();
            kp.DrawString(name, drawFont, drawBrush, x - 10, y - 20, drawFormat);
        }


    }
}
